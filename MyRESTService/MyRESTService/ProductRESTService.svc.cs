﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MyRESTService
{
    public class ProductRESTService : IProductRESTService
    {

        PostoEntities contexto = new PostoEntities();

        public ProductRESTService()
        {
            contexto.Configuration.ProxyCreationEnabled = false;
        }

        public Pessoa InsertPessoa(Pessoa p)
        {
            try
            {
                Pessoa pessoa = contexto.Pessoa.FirstOrDefault(x => x.usuario == p.usuario || x.email == p.email);

                if (pessoa != null)
                {
                    Pessoa retorno = new Pessoa();
                    retorno.usuario = pessoa.usuario;
                    retorno.email = pessoa.email;
                    return retorno;
                }

                contexto.Pessoa.Add(p);

                contexto.SaveChanges();
                return new Pessoa();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Pessoa Login(Pessoa p)
        {
            try
            {
                Pessoa pessoa = contexto.Pessoa.FirstOrDefault(x => x.usuario == p.usuario);

                if (pessoa == null || pessoa.senha != p.senha)
                    return new Pessoa();

                pessoa.senha = "";

                return pessoa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Posto> BuscarPostos(Posto p)
        {
            try
            {
                List<Posto> postos = new List<Posto>();
                if (p.id != 0)
                {
                    postos = contexto.Posto.Where(x => x.id == p.id).ToList();
                    return postos;
                }
                if (!String.IsNullOrEmpty(p.nome))
                {
                    if (postos.Count == 0)
                    {
                        postos = contexto.Posto.Where(x => x.nome.Contains(p.nome)).ToList();
                    }
                    else
                    {
                        postos = postos.Where(x => x.nome.Contains(p.nome)).ToList();
                    }
                }
                if (!String.IsNullOrEmpty(p.rua))
                {
                    if (postos.Count == 0)
                    {
                        postos = contexto.Posto.Where(x => x.rua.Contains(p.rua)).ToList();
                    }
                    else
                    {
                        postos = postos.Where(x => x.rua.Contains(p.rua)).ToList();
                    }
                }
                if (p.numero != null && p.numero != 0)
                {
                    if (postos.Count == 0)
                    {
                        postos = contexto.Posto.Where(x => x.numero == p.numero).ToList();
                    }
                    else
                    {
                        postos = postos.Where(x => x.numero == p.numero).ToList();
                    }
                }
                if (p.bandeira != null && p.bandeira != 0)
                {
                    if (postos.Count == 0)
                    {
                        postos = contexto.Posto.Where(x => x.bandeira == p.bandeira).ToList();
                    }
                    else
                    {
                        postos = postos.Where(x => x.bandeira == p.bandeira).ToList();
                    }
                }
                //filtro por distancia ainda nao esta pronto
                //if (p.latitude != null && p.longitude != null)
                //{
                //    GeoCoordinate local = new GeoCoordinate(p.latitude.Value, p.longitude.Value);
                //    if (postos.Count == 0)
                //    {

                //        postos = contexto.Posto.Where(x => local.GetDistanceTo(new GeoCoordinate(x.latitude.Value, x.longitude.Value)) <= 2000).ToList();
                //    }
                //    else
                //    {
                //        postos = postos.Where(x => local.GetDistanceTo(new GeoCoordinate(x.latitude.Value, x.longitude.Value)) <= 2000).ToList();
                //    }

                //}

                return postos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Product> GetProductList()
        {

            SqlConnection conn;

            Pessoa pessoa = new Pessoa();

            return Products.Instance.ProductList;
        }
    }
}
