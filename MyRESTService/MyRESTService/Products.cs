﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MyRESTService
{
    [DataContract]
    public class Product
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public String nome { get; set; }
    }

    public partial class Products
    {
        private static readonly Products _intance = new Products();

        private Products() { }

        public static Products Instance
        {
            get { return _intance; }
        }
        public List<Product> ProductList
        {
            get { return products; }
        }

        private List<Product> products = new List<Product>()
        {
            new Product() {id =1,nome="um" },
            new Product() {id =2,nome="dois" },
            new Product() {id =3,nome="tres" },
            new Product() {id =4,nome="quatro" }
        };

    }
}